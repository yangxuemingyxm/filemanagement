package com.ezui.service;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;

import com.documentum.fc.client.IDfCollection;
import com.documentum.fc.client.IDfSession;
import com.documentum.fc.client.IDfSysObject;
import com.documentum.fc.common.DfException;
import com.documentum.fc.common.DfId;
import com.emc.common.java.utils.ArgumentParser;
import com.emc.d2.api.methods.D2Method;
import com.emc.d2.api.methods.D2methodBean;
import com.emc.d2.api.methods.ID2Method;
import com.ezui.documentum.utils.CollectionUtils;
import com.ezui.documentum.utils.FolderUtils;
import com.ezui.utils.IDFSessionUtils;

public class ExportFile implements ID2Method{
	public static void main(String[] args){
		IDfSession session=null;
		try {
			System.out.println("创建session对象");
			session=IDFSessionUtils.login("DCTMTEST", "dmadmin", "Passw0rd", null);
			System.out.println("执行exportObject方法");
			boolean flag=new ExportFile().exportObject(session);
			System.out.println("执行结果为"+flag);
		} catch (Exception  e) {
			e.printStackTrace();
		}
	}


	/**
	 * @author karashorkPam
	 * @param session
	 * @return
	 */
	public boolean exportObject(IDfSession session) throws DfException, IOException
	{

		return true;
	}


	public D2methodBean  execute(IDfSession session, IDfSysObject object,
			Locale arg2, ArgumentParser arg3) throws Exception {
		// TODO Auto-generated method stub
		D2methodBean localD2methodBean = new D2methodBean(D2Method.RETURN_SUCCESS_STR, null);
		IDfCollection coll=null;
		String action="checkin";
		String mainFormat = "msw";
		try {
			//根据id得到对象
			object =(IDfSysObject) session.getObject(new DfId("0901e240800731a3"));
			System.out.println("当前文件对象的原路径为"+FolderUtils.getFolderPath(session, object.getFolderId(0).getId()));
			coll = object.getRenditions("full_format");
			//如果属性为checkin
			if(false){
				while (coll.next()) {
					String format = coll.getString("full_format");
					if(format.startsWith(mainFormat))
					{
						String tmpFormat="doc";
						String sql="select dos_extension from dm_format where name='"+format+"'";
						List<String> list=CollectionUtils.executeDQL(session, sql, "dos_extension");
						if(list!=null&&list.size()>0){
							tmpFormat=list.get(0);
						}
						//获取文件目录
						String tmpPath=FolderUtils.getFolderPath(session, object.getFolderId(0).getId());
						System.out.println("文件名为"+object.getObjectName());
						//创建本地文件目录
						System.out.println("创建本地文件目录");
						StringBuffer filePath=new StringBuffer("D:");
						File filePathDir=new File(filePath.toString()+"//"+tmpPath);
						filePathDir.mkdirs();
						//创建本地文件对象
						System.out.println("创建本地文件");
						File newFile = new File(filePath.toString()+tmpPath+"//"+object.getObjectName()+"."+tmpFormat);
						if(!newFile.exists()){
							newFile.createNewFile();
						}
						System.out.println("文件在本地的存放路径是:+"+newFile.getPath());
						//开始执行导入方法
						System.out.println("开始执行导入方法");
						object.getFileEx(newFile.getAbsolutePath(),format,0,false);
					}	

				}
			}else{
				while (coll.next()) {
					String format = coll.getString("full_format");
					String tmpFormat="doc";
					String tempBackupsFormat="pdf";
					//导出word
					if(format.startsWith(mainFormat))
					{
						String sql="select dos_extension from dm_format where name='"+format+"'";
						List<String> list=CollectionUtils.executeDQL(session, sql, "dos_extension");
						if(list!=null&&list.size()>0){
							tmpFormat=list.get(0);
						}
						//获取文件目录
						String tmpPath=FolderUtils.getFolderPath(session, object.getFolderId(0).getId());
						System.out.println("文件名为"+object.getObjectName());
						//创建本地文件目录
						System.out.println("创建本地文件目录");
						StringBuffer filePath=new StringBuffer("D:");
						File filePathDir=new File(filePath.toString()+"//"+tmpPath);
						filePathDir.mkdirs();
						//创建本地文件对象
						System.out.println("创建本地文件");
						File newFile = new File(filePath.toString()+tmpPath+"//"+object.getObjectName()+"."+tmpFormat);
						if(!newFile.exists()){
							newFile.createNewFile();
						}
						System.out.println("文件在本地的存放路径是:+"+newFile.getPath());
						//开始执行导入方法
						System.out.println("开始执行导入方法");

						object.getFileEx(newFile.getAbsolutePath(),format,0,false);
					}
					//导出pdf
					if(format.startsWith(tempBackupsFormat)){
						String sql="select dos_extension from dm_format where name='"+format+"'";
						List<String> list=CollectionUtils.executeDQL(session, sql, "dos_extension");
						if(list!=null&&list.size()>0){
							tmpFormat=list.get(0);
						}
						//获取文件目录
						String tmpPath=FolderUtils.getFolderPath(session, object.getFolderId(0).getId());
						System.out.println("文件名为"+object.getObjectName());
						//创建本地文件目录
						System.out.println("创建本地文件目录");
						StringBuffer filePath=new StringBuffer("D:");
						File filePathDir=new File(filePath.toString()+"//"+tmpPath);
						filePathDir.mkdirs();
						//创建本地文件对象
						System.out.println("创建本地文件");
						File newFile = new File(filePath.toString()+tmpPath+"//"+object.getObjectName()+"."+tmpFormat);
						if(!newFile.exists()){
							newFile.createNewFile();
						}
						System.out.println("文件在本地的存放路径是:"+newFile.getPath());
						//开始执行导入方法
						System.out.println("开始执行导入方法");
						object.getFileEx(newFile.getAbsolutePath(),format,0,false);
					}

				}
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return localD2methodBean;
	}
}


