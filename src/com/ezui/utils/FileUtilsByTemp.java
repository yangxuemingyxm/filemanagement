package com.ezui.utils;


import com.documentum.fc.client.IDfFolder;
import com.documentum.fc.client.IDfSession;
import com.documentum.fc.client.IDfSysObject;
import com.documentum.fc.common.DfException;
import com.ezui.documentum.utils.StringUtils;

public  class FileUtilsByTemp {
	public static String createFolderByPath(IDfSession session, String path,
			String folderType) throws DfException {
		if (StringUtils.isEmpty(path))
			return "";
		// 格式化要创建的文件夹路径为�?/“开�?
		if (!path.startsWith("/"))
			path = "/" + path;
		IDfFolder folder = session.getFolderByPath(path);
		if (folder != null)
			return path;

		String[] splitPathArray = path.split("/");
		StringBuffer allPath = new StringBuffer();
		allPath.append("/");
		for (String splitPath : splitPathArray) {
			if (StringUtils.isEmpty(splitPath))
				continue;
			allPath.append(splitPath);
			folder = (IDfFolder) session.getFolderByPath(allPath.toString());
			if (folder == null) {// 文件夹不存在
				try {
					String parentPath = allPath.substring(0,
							allPath.lastIndexOf("/"));
					if (StringUtils.isEmpty(parentPath)) {// 为文件柜
						folder = (IDfFolder) session.newObject("dm_cabinet");
					} else {// 为文件夹
						folder = (IDfFolder) session.newObject(folderType);
						folder.link(parentPath);
					}
					folder.setObjectName(splitPath);
					folder.save();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			allPath.append("/");
		}
		if (allPath.lastIndexOf("/") > 0)
			return allPath.substring(0, allPath.length() - 1);
		return allPath.toString();
	}
	public static void setFileContent(IDfSysObject doc, String fileName){
		try{

			String fileNameExt=fileName.substring(fileName.lastIndexOf(".")+1,fileName.length());
			System.out.println("文件后缀为========"+fileNameExt);
			String format="pdf";
			if(fileNameExt!=null && !"".equals(fileNameExt)){
				if("pdf".equalsIgnoreCase(fileNameExt)){
					format="pdf";
				}else if("rar".equalsIgnoreCase(fileNameExt)){
					format="rar";
				}else if("xls".equalsIgnoreCase(fileNameExt)){
					format="excel8book";
				}else if("zip".equalsIgnoreCase(fileNameExt)){
					format="zip";
				}else if("tif".equalsIgnoreCase(fileNameExt)){
					format="tiff";
				}else if("bat".equalsIgnoreCase(fileNameExt)){
					format="bat";
				}else if("doc".equalsIgnoreCase(fileNameExt)){
					format="msw8";
				}else if("docx".equalsIgnoreCase(fileNameExt)){
					format="msw12";
				}
				doc.setContentType(format);
			}else{
				doc.setContentType(format);
			}
			System.out.println("开始执行setFile方法");
			doc.setFile(fileName);
			System.out.println("开始执行save方法");
			doc.save();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void setFileRendition(IDfSysObject doc, String renditionPath){
		try{
			String fileName =renditionPath.substring(renditionPath.lastIndexOf("/")+1,  renditionPath.length());
			String fileNameExt=fileName.substring(fileName.lastIndexOf(".")+1,fileName.length());
			System.out.println("文件后缀为========"+fileNameExt);
			String format="pdf";
			if(fileNameExt!=null && !"".equals(fileNameExt)){
				if("pdf".equalsIgnoreCase(fileNameExt)){
					format="pdf";
				}else if("rar".equalsIgnoreCase(fileNameExt)){
					format="rar";
				}else if("xls".equalsIgnoreCase(fileNameExt)){
					format="excel8book";
				}else if("zip".equalsIgnoreCase(fileNameExt)){
					format="zip";
				}else if("tif".equalsIgnoreCase(fileNameExt)){
					format="tiff";
				}else if("bat".equalsIgnoreCase(fileNameExt)){
					format="bat";
				}else if("doc".equalsIgnoreCase(fileNameExt)){
					format="msw8";
				}else if("docx".equalsIgnoreCase(fileNameExt)){
					format="msw12";
				}
			}
			doc.addRendition(renditionPath, format);
			System.out.println("开始执行save方法");
			doc.save();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
