package com.ezui.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import com.documentum.fc.common.DfLogger;
import com.ezui.documentum.utils.ClassPathUtil;

public class PropertiesUtil {

	private static PropertiesUtil current = new PropertiesUtil();
	private String docbase=null;
	private String password = null;
	private String user = null;
	private String D2URL = null;
	private String postURL = null;
	private String notifyRole = null;
	private String smtpHost = null;
	private String smtpPort = null;
	private String smtpUser = null;
	private String smtpPassword = null;
	private String smtpAuth = null;
	private String smtpStarttls = null;
	private String smtpSSL = null;
	private String smtpFrom = null;
			
	
			
	public PropertiesUtil() {
		String filePath = ClassPathUtil.getResourcePath(".." + File.separator + "config" + File.separator + "env.properties");
		Properties ps = loadConfFile(filePath);
		docbase = ps.getProperty("henlius.docbase", docbase);
		user = ps.getProperty("henlius.user", user);
		password = ps.getProperty("henlius.password", password);
		D2URL = ps.getProperty("henlius.D2URL", D2URL);
		postURL = ps.getProperty("henlius.postURL", postURL);
		notifyRole = ps.getProperty("henlius.notifyRole", notifyRole);
		
		smtpHost = ps.getProperty("mail.smtp.host", smtpHost);
		smtpPort = ps.getProperty("mail.smtp.port", smtpPort);
		smtpUser = ps.getProperty("mail.smtp.user", smtpUser);
		smtpPassword = ps.getProperty("mail.smtp.password", smtpPassword);
		smtpAuth = ps.getProperty("mail.smtp.auth", smtpAuth);
		smtpStarttls = ps.getProperty("mail.smtp.starttls.enable", smtpStarttls);
		smtpSSL = ps.getProperty("mail.smtp.ssl.enable", smtpSSL);
		smtpFrom  = ps.getProperty("mail.smtp.from", smtpFrom);
	}


	public static Properties loadConfFile(String confFilePath) {
		Properties properties = new Properties();
		FileInputStream fis = null;
		try {
			fis = new FileInputStream(confFilePath);
			properties.load(fis);
			return properties;
		} catch (IOException e) {
			DfLogger.error(PropertiesUtil.class, "PROP_ERROR", null, e);
			return null;
		} finally {
			if (fis != null) {
				try {
					fis.close();
				} catch (IOException e) {
				}
			}
		}
	}
	

	public PropertiesUtil getCurrent(){
		return current;
	}
	
	public String getSMTPHost() {
		return smtpHost;
	}
	
	public String getSMTPPort() {
		return smtpPort;
	}
	
	public String getSMTPUser() {
		return smtpUser;
	}
	
	public String getSMTPPassword() {
		return smtpPassword;
	}
	
	public String getSMTPAuth() {
		return smtpAuth;
	}
	
	public String getSMTPSSL() {
		return smtpSSL;
	}
	
	public String getSMTPStarttls() {
		return smtpStarttls;
	}
	
	public String getSMTPFrom() {
		return smtpFrom;
	}
	
	public String getNotifyRole(){
		return notifyRole;
	}
	
	public String getDocbase(){
		return docbase;
	}
	
	public String getUser(){
		return user;
	}
	
	public String getPassword(){
		return password;
	}

	public String getD2URL(){
		return D2URL;
	}
	
	public String getPostURL(){
		return postURL;
	}
	
}
