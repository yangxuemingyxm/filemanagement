package com.ezui.utils;


import com.documentum.fc.client.DfClient;
import com.documentum.fc.client.IDfClient;
import com.documentum.fc.client.IDfSession;
import com.documentum.fc.client.IDfSessionManager;
import com.documentum.fc.common.DfException;
import com.documentum.fc.common.DfLoginInfo;
import com.documentum.fc.common.IDfLoginInfo;

public class IDFSessionUtils  {


	
	
	public static IDfSession login(String docbase, String username, String password, String domain) throws DfException {
		IDfSession session = null;
		if (docbase == null || username == null)
			return null;
		IDfClient dfClient = DfClient.getLocalClient();
		if (dfClient != null) {
			IDfLoginInfo li = new DfLoginInfo();
			li.setUser(username);
			li.setPassword(password);
			li.setDomain(domain);
			IDfSessionManager sessionMgr = dfClient.newSessionManager();
			sessionMgr.setIdentity(docbase, li);
			session = sessionMgr.getSession(docbase);
		}
		
		return session;
	}
}
